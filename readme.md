# Instruções

## Iniciar o projeto
<p> Faça o clone do projeto no seu computador ou baixe </p>
<p> E logo após execute o comando dentro da pasta onde tiver
<strong>docker-composer.yaml</strong>

```
docker-compose up -d --build
```
<p>Para visualizar que funcionou</p>

```
docker ps -a 
```

<p> Apos gerar o seu ambiente wordpress no docker, aguarde uns instantes até tudo está okay </p>

<p> Está configurado dessa maneira os acesso</p>

```
 - Sua aplicação wordpress
    localhost:8091
 - Phpmyadmin
    localhost:8093
```

## Configurar container do Wordpress
<p> Caso mude o nome do seu container onde vai ficar os arquivos. Nesse exemplo o padrão
é <strong>wordpress</strong> para  <strong>worpdress_my_app</strong>. Não
esqueça de alterar o arquivo nginx.conf na linha</p>

```
fastcgi_pass wordpress_my_app:9000;

```

## Configurar no container Mysql
<p>No comando</p>

```
 environment:
      - MYSQL_DATABASE=wordpress
      - MYSQL_ROOT_PASSWORD=wordpress
      - MYSQL_USER=novo_root
```
<p> Você pode Alterar o nome do banco de dados para sua necessidade no <strong>MYSQL_DATABASE</strong> e a senha padrão para acesso no <strong> MYSQL_ROOT_PASSWORD </strong>. O <strong>MYSQL_USER</strong> é para mudar o padrão que é <strong>root</strong> caso sinta necessidade.

## Configurar o phpmyadmin
<p> São variáveis para conectar  o <strong>phpmyadmin</strong> ao banco de dados mysql</p>

```
environment:
      - PMA_HOST=nome_do_container_banco_de_dados
      - PMA_USER=root
      - PMA_PASSWORD=aqui_e_o_password
```

No nosso caso já esta configurado o <strong> PMA_PASSWORD </strong> e o  mesmo <strong> MYSQL_ROOT_PASSWORD </strong>. E o nosso <strong>HOST</strong> nome do nosso container do banco de dados
no nosso caso <strong>db</strong>.

## Configurar Wordpress API
<p> É importante ter essa parte do código no nginx.conf para funcionar
o wordpress em formato de api </p>

```
    location / {
    # http://v2.wp-api.org/guide/problems/#query-parameters-are-ignored
    try_files $uri $uri/ /index.php$is_args$args;
    }

    location ~ ^/wp-json/ {
    # if permalinks not enabled
    rewrite ^/wp-json/(.*?)$ /?rest_route=/$1 last;
    }
```

## Na parte instalar plugin no wordpress
<P> Na situação foi um plugin Autenticação com JWT
<p> As vezes pede acesso ftp para resolver esse problema </p>

<p>Com uma pesquisa rápida no Google você encontra a solução de mudar o dono da pasta do site (no momento deve estar “root” ou “seu-usuario”):</p>

```
$ sudo chown -R www-data pasta-do-site
```
```
$ sudo chmod -R g+w pasta-do-site
```

<p>A primeira linha altera recursivamente o dono da pasta para “www-data” (geralmente esse é o usuário atribuído ao seu localhost, se não for, troque o nome) e a segunda linha torna isso verdade para as novas subpastas, caso crie alguma.</p>

<p>O problema dessa solução é que muito provavelmente você não poderá mais criar ou alterar manualmente pastas e arquivos (através da interface gráfica, por exemplo).</p>